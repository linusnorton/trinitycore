# MIT License
# Copyright (c) 2017-2022 Nicola Worthington <nicolaw@tfb.net>
# https://gitlab.com/nicolaw/trinitycore

.PHONY: test build mapdata clean run tdb sql pull
.PHONY: build-all pull-all publish-all
.DEFAULT_GOAL := help

# Flavour image to build. Defaults to "sql" for easy use with bundled
# docker-compose.yaml.
#FLAVOUR=slim
FLAVOUR=sql

DOCKERFILE = Dockerfile

# Where this Makefile resides.
MAKEFILE = $(firstword $(MAKEFILE_LIST))
MAKEFILE_DIR = $(dir $(MAKEFILE))

# What realm ID and port should the worldserver identify itself as.
WORLDSERVER_REALM_ID = 2
WORLDSERVER_PORT = 8085
WORLDSERVER_IP = $(shell hostname -I | egrep -o '[0-9\.]{7,15}' | grep -v ^127. | head -1)
WORLDSERVER_NAME = $(shell hostname -s | sed -e "s/\b\(.\)/\u\1/g")

# Container label information.
VCS_REF = $(shell git rev-parse HEAD)
BUILD_DATE = $(shell date +'%Y-%m-%d %H:%M:%S%z')
BUILD_VERSION = $(shell cat VERSION)

# Where to pull the upstream TrinityCore source from.
GITHUB_API = https://api.github.com
TC_GITHUB_REPO = TrinityCore/TrinityCore
TC_GIT_BRANCH = 3.3.5
TC_GIT_REPO = https://github.com/$(TC_GITHUB_REPO).git

# What to call the resulting container image.
IMAGE_TAG = $(TC_GIT_BRANCH)-$(FLAVOUR)
CI_REGISTRY ?= docker.io
IMAGE_REGISTRY = ${CI_REGISTRY}
CI_REGISTRY_IMAGE ?= ${IMAGE_REGISTRY}/nicolaw/trinitycore
IMAGE_REPO = ${CI_REGISTRY_IMAGE}
IMAGE_NAME = $(IMAGE_REPO):$(IMAGE_TAG)

# SHA of upstream TrinityCore source that has been built (used for publishing).
BUILT_UPSTREAM_SHA = $(shell docker run --rm $(IMAGE_NAME) cat /.git-rev-short)

# Full database dump for the worldserver to populate the "world" database table with.
# https://github.com/TrinityCore/TrinityCore/releases
TDB_FULL_URL = $(shell $(MAKEFILE_DIR)gettdb -n "$(TC_GIT_BRANCH)")
TDB_7ZIP_FILE = $(notdir $(TDB_FULL_URL))
TDB_SQL_FILE = $(patsubst %.7z,%.sql,$(TDB_7ZIP_FILE))

# Location of WoW game client files, used to generate worldserver map data.
GAME_CLIENT_DIR = ./World_of_Warcraft/

# Directories expected to be generated for worldserver map data.
MAP_DATA_DIR = ./mapdata/

# MPQ game data files use to generate the worldserver map data.
MPQ_LOCALE = $(notdir $(shell find $(GAME_CLIENT_DIR)Data/ -mindepth 1 -maxdepth 1 -type d 2>/dev/null || echo enUS))
MPQ = $(addprefix $(GAME_CLIENT_DIR)Data/, $(addsuffix .MPQ, \
	common expansion patch-3 patch-2 patch common-2 lichking \
	$(MPQ_LOCALE)/lichking-speech-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/expansion-speech-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/lichking-locale-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/expansion-locale-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/base-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/patch-$(MPQ_LOCALE)-2 \
	$(MPQ_LOCALE)/backup-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/speech-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/patch-$(MPQ_LOCALE)-3 \
	$(MPQ_LOCALE)/patch-$(MPQ_LOCALE) \
	$(MPQ_LOCALE)/locale-$(MPQ_LOCALE) ) )

.INTERMEDIATE: $(MPQ)

sql:
	$(MAKEFILE_DIR)getsql "$(TC_GIT_BRANCH)"

tdb: $(TDB_SQL_FILE)

$(TDB_SQL_FILE): $(TDB_7ZIP_FILE)
	test -s "$@" || { cd $(MAKEFILE_DIR) && 7zr x -y -- "$<"; }
	test -s "$@" && touch "$@"

$(TDB_7ZIP_FILE):
	$(MAKEFILE_DIR)gettdb "$(TC_GIT_BRANCH)"

help:
	@echo ""
	@echo "Use 'make build' to build the TrinityCore container image."
	@echo "Use 'make pull' to download the TrinityCore container image instead of building it."
	@echo "Use 'make shell' to launch an interactive shell inside the TrinityCore container."
	@echo "Use 'make mapdata' to generate worldserver map data from the WoW game client."
	@echo "Use 'make run' to launch the TrinityCore servers using docker-compose."
	@echo "Use 'make tdb' to download the full worldserver database SQL dump. (Optional)"
	@echo "Use 'make sql' to download the SQL files omitted in the 'slim' container. (Optional)"
	@echo "Use 'make dumpdb' to create an SQL dump backup of the dataases."
	@echo "Use 'make test' to test the TrinityCore servers start using docker-compose."
	@echo "Use 'make clean' to destroy ALL container images and MySQL database volume."
	@echo ""
	@echo "Refer to the following documents for additional help:"
	@echo "  https://gitlab.com/nicolaw/trinitycore/-/blob/master/README.md"
	@echo "  https://gitlab.com/nicolaw/trinitycore/-/blob/master/GettingStarted.md"
	@echo "  https://gitlab.com/nicolaw/trinitycore/-/blob/master/Building.md"
	@echo ""
	@echo "  https://www.trinitycore.org/"
	@echo "  https://trinitycore.info/"
	@echo "  https://trinitycore.net/"
	@echo "  https://trinitycore.atlassian.net/wiki/spaces/tc/"
	@echo ""

clean:
	@while [ -z "$$CONFIRM_CLEAN" ]; do \
		read -r -p "Are you sure you want to delete ALL built and intermediate container images and database volumes? [y/N]: " CONFIRM_CLEAN; \
	done; [ "$$CONFIRM_CLEAN" = "y" ]
	docker-compose down || true
	docker-compose rm -sfv || true
	docker volume rm trinitycore_db-data || true
	docker image rm "$(docker image ls --filter "label=org.opencontainers.image.title=nicolaw/trinitycore-intermediate-build" --quiet)"
	docker image rm "$(docker image ls --filter "label=org.opencontainers.image.title=nicolaw/trinitycore" --quiet)"
	$(RM) .tdb-full-url.* mysql.log worldserver.log authserver.log

run: $(MAP_DATA_DIR)mmaps sql/custom/auth/0002-fix-realmlist.sql
	docker-compose up

# Create custom SQL file to be imported on first run that updates the IP
# address of the worldserver to be something other than just localhost.
# https://trinitycore.atlassian.net/wiki/spaces/tc/pages/2130016/realmlist
sql/custom/auth/0002-fix-realmlist.sql:
	printf 'REPLACE INTO realmlist (id,name,address,port) VALUES ("%s","%s","%s","%s");\n' \
		"$(WORLDSERVER_REALM_ID)" "$(WORLDSERVER_NAME)" "$(WORLDSERVER_IP)" "$(WORLDSERVER_PORT)" > "$@"

build:
	docker build \
	-f $(DOCKERFILE) $(MAKEFILE_DIR) -t $(IMAGE_NAME) \
	--build-arg FLAVOUR="$(FLAVOUR)" \
	--build-arg TC_GIT_BRANCH="$(TC_GIT_BRANCH)" \
	--build-arg TC_GIT_REPO="$(TC_GIT_REPO)" \
	--build-arg VCS_REF="$(VCS_REF)" \
	--build-arg BUILD_DATE="$(BUILD_DATE)" \
	--build-arg BUILD_VERSION="$(BUILD_VERSION)" \
	--build-arg TDB_FULL_URL="$(TDB_FULL_URL)"
	BUILD_UPSTREAM_SHA="$$(docker run --rm $(IMAGE_NAME) cat /.git-rev)" \
		&& echo "FROM $(IMAGE_NAME)" | docker build - -t $(IMAGE_NAME) \
		--label org.opencontainers.image.trinitycore.revision="$$BUILD_UPSTREAM_SHA"
	docker inspect $(IMAGE_NAME) | jq -r '.[0].Config.Labels'

build-all:
	$(MAKE) -f $(MAKEFILE) build FLAVOUR=slim
	$(MAKE) -f $(MAKEFILE) build FLAVOUR=sql

pull:
	docker pull $(IMAGE_NAME)

pull-all:
	$(MAKE) -f $(MAKEFILE) pull FLAVOUR=slim
	$(MAKE) -f $(MAKEFILE) pull FLAVOUR=sql

publish:
	docker tag $(IMAGE_NAME) $(IMAGE_NAME)-$(BUILT_UPSTREAM_SHA)
	docker tag $(IMAGE_NAME) $(IMAGE_REPO):$(FLAVOUR)
	docker push $(IMAGE_NAME)
	docker push $(IMAGE_NAME)-$(BUILT_UPSTREAM_SHA)
	docker push $(IMAGE_REPO):$(FLAVOUR)

publish-all:
	$(MAKE) -f $(MAKEFILE) publish FLAVOUR=slim
	$(MAKE) -f $(MAKEFILE) publish FLAVOUR=sql
	docker tag $(IMAGE_NAME) $(IMAGE_REPO):$(BUILT_UPSTREAM_SHA)
	docker tag $(IMAGE_NAME) $(IMAGE_REPO):$(TC_GIT_BRANCH)
	docker tag $(IMAGE_NAME) $(IMAGE_REPO):latest
	docker push $(IMAGE_REPO):$(BUILT_UPSTREAM_SHA)
	docker push $(IMAGE_REPO):$(TC_GIT_BRANCH)
	docker push $(IMAGE_REPO):latest

shell:
	docker run --rm -it $(IMAGE_NAME)

test:
	tests/docker-compose.sh

# Explicitly remind the user where to copy their World of Warcraft game client
# files if they are missing.
%.MPQ:
	$(error Missing $@ necessary to generate worldserver map data; please copy \
		your World of Warcraft game client in to the $(GAME_CLIENT_DIR) directory)

# Generate worldserver map data from World of Warcraft game client data inside a
# Docker container.
mapdata: $(MAP_DATA_DIR)mmaps

$(MAP_DATA_DIR)Cameras $(MAP_DATA_DIR)dbc $(MAP_DATA_DIR)maps: $(MPQ)
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) mapextractor -i /wow -o /mapdata -e 7 -f 0

$(MAP_DATA_DIR)Buildings: $(MPQ)
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) vmap4extractor -l -d /wow/Data

$(MAP_DATA_DIR)vmaps: $(MPQ) $(MAP_DATA_DIR)Buildings
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) vmap4assembler /mapdata/Buildings /mapdata/vmaps

$(MAP_DATA_DIR)mmaps: $(MPQ) $(MAP_DATA_DIR)maps $(MAP_DATA_DIR)vmaps
	docker run --rm -v $(abspath $(GAME_CLIENT_DIR)):/wow -v $(abspath $(MAP_DATA_DIR)):/mapdata -w /mapdata $(IMAGE_NAME) mmaps_generator

# Convenience database backup target.
MYSQL_USER = trinity
MYSQL_PWD = trinity
MYSQL_HOST = 127.0.0.1
MYSQL_TCP_PORT = 3306
MYSQLDUMP_CMD = MYSQL_PWD=$(MYSQL_PWD) mysqldump -h $(MYSQL_HOST) -P $(MYSQL_TCP_PORT) -u $(MYSQL_USER) --hex-blob

.PHONY: dumpdb
dumpdb: auth-realmlist.sql auth-account.sql auth.sql characters.sql db-auth.sql db-characters.sql
	@echo
	ls -lah $^
	@echo
	@echo "Use 'make db-world.sql' to to backup the world database (usually only necessary when explicitly customised)."
	@echo

.PHONY: auth.sql characters.sql
auth.sql characters.sql:
	$(MYSQLDUMP_CMD) --no-create-info --insert-ignore --compact --no-create-db --ignore-table=$(basename $@).build_info --ignore-table=$(basename $@).updates --ignore-table=$(basename $@).updates_include --ignore-table=$(basename $@).uptime --databases $(basename $@) > $@

.PHONY: auth-realmlist.sql auth-account.sql
auth-realmlist.sql auth-account.sql:
	$(MYSQLDUMP_CMD) --no-create-info --replace --compact --no-create-db auth $(shell echo $(basename $@) | sed 's/^auth-//') > $@

.PHONY: db-auth.sql db-world.sql db-characters.sql
db-auth.sql db-world.sql db-characters.sql:
	$(MYSQLDUMP_CMD) $(shell echo $(basename $@) | sed 's/^db-//') > $@

