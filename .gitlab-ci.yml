# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/
# https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html
# https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html
# https://docs.gitlab.com/ee/user/application_security/dependency_scanning/analyzers.html
# https://docs.gitlab.com/ee/user/application_security/secret_detection/index.html
# https://docs.gitlab.com/ee/user/application_security/iac_scanning/index.html
# https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#example-configuration
include:
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST-IaC.latest.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml

stages:
  - test
  - build
  - container-test
  - deploy

variables:
  TC_GIT_BRANCH: 3.3.5
  BUILD_MAPDATA:
    value: "false"
    description: "Set to 'true' to generate and publish world server height map data archive."

# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
container_scanning:
  stage: container-test
  variables:
    CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA
    SECURE_LOG_LEVEL: debug
    GIT_STRATEGY: fetch

# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST-IaC.latest.gitlab-ci.yml
kics-iac-sast:
  variables:
    # https://docs.kics.io/latest/configuration-file/#environment_variables
    # https://docs.kics.io/latest/queries/cloudformation-queries/
    # 105ba098-1e34-48cd-b0f2-a8a43a51bf9b   False positive; no ALB is in use
    # 2b1d4935-9acf-48a7-8466-10d18bf51a69   Ignoring; RDS deliberately deployed to single AZ for lower costs
    # 1cc2fbd7-816c-4fbf-ad6d-38a4afa4312a   Ignoring; EC2 instances need to make outbound HTTP requests for package installation
    KICS_EXCLUDE_QUERIES: "105ba098-1e34-48cd-b0f2-a8a43a51bf9b,2b1d4935-9acf-48a7-8466-10d18bf51a69,1cc2fbd7-816c-4fbf-ad6d-38a4afa4312a"

.dind:
  image: docker:20.10.12-alpine3.15
  services:
    - docker:20.10.12-dind-alpine3.15
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  tags:
    - sansa
    - trinitycore
  before_script:
    - apk add make jq git curl tar xz findutils
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  cache: &global_cache
    key: $CI_COMMIT_SHA
    untracked: true
    paths:
      - docker_save_*.tar
      - .git-rev*
    policy: pull-push

build:
  extends: .dind
  stage: build
  script:
    - make build-all
    # Save build variables for use in other jobs.
    - |-
      TC_GIT_SHA=$(docker run --rm "${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim" cat /.git-rev-short)
      cat <<EOF > build.env
      TC_GIT_SHA=$TC_GIT_SHA
      TDB_FULL_URL=$(cat .tdb-full-url.*)
      MAPDATA_PACKAGE_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/trinitycore-mapdata/${TC_GIT_BRANCH}-${TC_GIT_SHA}/trinitycore-mapdata-${TC_GIT_BRANCH}-${TC_GIT_SHA}.tar.xz
      MAPDATA_PACKAGE_FILE=trinitycore-mapdata-${TC_GIT_BRANCH}-${TC_GIT_SHA}.tar.xz
      EOF
    # Push to GitLab container registry for container_scanning job.
    - docker run --rm ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim cat /.git-rev > .git-rev
    - docker run --rm ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim cat /.git-rev-short > .git-rev-short
    - docker save -o docker_save_${CI_COMMIT_REF_SLUG}_${TC_GIT_BRANCH}-slim.tar ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim
    - docker save -o docker_save_${CI_COMMIT_REF_SLUG}_${TC_GIT_BRANCH}-sql.tar ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-sql
    - docker tag ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-sql $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA
  artifacts:
    reports:
      dotenv: build.env

test:
  extends: .dind
  stage: container-test
  cache:
    <<: *global_cache
    policy: pull
  script:
    - &docker-load-from-cache
      for tar in docker_save_*.tar ; do docker load -i $tar ; done
    - docker run ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim authserver --version
    - docker run ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim worldserver --version
    - docker run ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim mapextractor --version
    # FIXME: #34 mmaps_generator, vmap4assembler nor vmap4extractor support
    #        --version in the upstream TrinityCore 3.3.5 branch.
    #        https://github.com/TrinityCore/TrinityCore/blob/3.3.5/src/tools/mmaps_generator/PathGenerator.cpp
    - docker run ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim mmaps_generator --version || true
    - docker run ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim vmap4assembler --version || true
    - docker run ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim vmap4extractor --version || true

docker-compose:
  extends: .dind
  stage: container-test
  cache:
    <<: *global_cache
    policy: pull
  artifacts:
    paths:
      - tests/*.log
    when: always
    expire_in: 1 month
  script:
    - apk add docker-compose
    - docker-compose rm -fsv
    - *docker-load-from-cache
    - &add-docker-io-image-tags |-
      for i in $(docker image list --format "{{.Repository}}:{{.Tag}}" --filter "reference=${CI_REGISTRY_IMAGE}:*")
      do
        docker tag $i $(echo $i | sed "s/^${CI_REGISTRY}/docker.io/")
      done
    - mkdir ./mapdata
    # TODO: #35 Dynamically set DOCKER_COMPOSE_MAPDATA_URL to latest or pull mapdata
    #       from the current pipeline. (Currently set under GitLab CI variables).
    - cd ./mapdata && curl -o - "${DOCKER_COMPOSE_MAPDATA_URL}" | tar -Jxf - && cd ..
    # TODO: Test that we can then talk to the worldserver SOAP API okay.
    - make test

mapdata:
  extends: .dind
  stage: deploy
  cache:
    <<: *global_cache
    policy: pull
  script:
    - |-
      if curl --fail -sSL -I -o /dev/null --header "JOB-TOKEN: $CI_JOB_TOKEN" "$MAPDATA_PACKAGE_URL"
      then
        echo "Package $MAPDATA_PACKAGE_URL already exists."
        exit 0
      fi
    - *docker-load-from-cache
    - mkdir -pv mapdata
    - chmod a+rwx mapdata
    - docker run --rm -v /World_of_Warcraft:/wow -v $PWD/mapdata:/mapdata ${CI_REGISTRY_IMAGE}:${TC_GIT_BRANCH}-slim genmapdata /wow /mapdata
    - tar -C mapdata -Jcvf "$MAPDATA_PACKAGE_FILE" .
    - du -sh mapdata/* trinitycore-mapdata-*.tar.xz
    - >
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$MAPDATA_PACKAGE_FILE" "$MAPDATA_PACKAGE_URL"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: always
    - if: '$BUILD_MAPDATA == "true"'
      when: always

publish:
  extends: .dind
  stage: deploy
  cache:
    <<: *global_cache
    policy: pull
  script:
    - *docker-load-from-cache
    - *add-docker-io-image-tags
    - cat $DOCKERHUB_TOKEN | docker login -u nicolaw --password-stdin docker.io
    - unset CI_REGISTRY_IMAGE CI_REGISTRY && make publish-all
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
